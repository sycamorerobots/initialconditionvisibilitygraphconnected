package uo.harish.prakash.sycamore.plugins.initialconditions;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;
import it.diunipi.volpi.sycamore.plugins.visibilities.VisibilityImpl;
import it.diunipi.volpi.sycamore.util.ApplicationProperties;
import it.diunipi.volpi.sycamore.util.PropertyManager;
import it.diunipi.volpi.sycamore.util.SycamoreUtil;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * 
 */

/**
 * @author harry
 *
 */
@PluginImplementation
public class InitialConditionVisibilityGraphConnected extends InitialConditionsImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private TYPE	_type				= TYPE.TYPE_2D;
	private String	_shortDescription	= "Visibility Graph Connected";
	private String	_description		= "Randomly connected visibility graph";
	private String	_pluginName			= "VisibilityGraphConnectedHP";

	// Generic Inherited properties
	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}
	// END OF Generic Inherited Properties

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		int minX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_X);
		int maxX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_X);
		int minY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_Y);
		int maxY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_Y);
		Point2D random;
		do {
			random = SycamoreUtil.getRandomPoint2D(minX, maxX, minY, maxY);
		} while (!isVisible(random, robots));

		return random;
	}

	private boolean isVisible(Point2D point, SycamoreRobotMatrix<Point2D> robots) {

		if (robots.robotsCount() < 1) {
			return true;
		}

		float radius = VisibilityImpl.getVisibilityRange() / 2;
		java.util.Iterator<SycamoreRobot<Point2D>> robotIterator = robots.iterator();

		while (robotIterator.hasNext()) {
			SycamoreRobot<Point2D> robot = robotIterator.next();
			if (robot.getGlobalPosition().distanceTo(point) < radius) {
				return true;
			}
		}

		return false;

	}
}
